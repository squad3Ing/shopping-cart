package com.ing.shoppingcart.service;

import java.util.List;

import com.ing.shoppingcart.dto.LoginRequestDto;
import com.ing.shoppingcart.dto.LoginResponseDto;
import com.ing.shoppingcart.dto.OrderResponseDto;

public interface UserService {

	LoginResponseDto loginUser(LoginRequestDto requestDto);

	public List<OrderResponseDto> getOrders(int userId, int pageSize, int pageNumber);

}
