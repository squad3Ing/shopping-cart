package com.ing.shoppingcart.service;

import java.io.Serializable;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ing.shoppingcart.dao.CartDao;
import com.ing.shoppingcart.dao.ProductDao;
import com.ing.shoppingcart.dto.CartRequestDto;
import com.ing.shoppingcart.dto.CartResponseDto;
import com.ing.shoppingcart.entity.CartItems;
import com.ing.shoppingcart.entity.Product;
import com.ing.shoppingcart.exception.ProductNotFoundException;
import com.ing.shoppingcart.exception.UserNotFoundException;
import com.ing.shoppingcart.util.ExceptionConstants;

@Service
public class CartServiceImpl implements CartService, Serializable {

	private static final long serialVersionUID = 1L;
	
	@Autowired
	CartDao cartDao;
	
	@Autowired
	ProductDao productDao;
	
	@Override
	public CartResponseDto addProductToCart(CartRequestDto cartRequestDto) {
		CartItems cartItems = new CartItems();
		Product response = productDao.findByProductId(cartRequestDto.getProductId());
		if(response == null) {
			throw new ProductNotFoundException(ExceptionConstants.PRODUCT_NOT_FOUND);
		}
		
		BeanUtils.copyProperties(cartRequestDto, cartItems);
		
		CartItems cartResponse = cartDao.save(cartItems);
		if(cartResponse == null) {
			throw new UserNotFoundException(ExceptionConstants.PRODUCT_NOT_FOUND);
		}
		Integer remainingQuantity = response.getQuantity()-cartRequestDto.getProductQuantity();
		response.setQuantity(remainingQuantity);
		productDao.save(response);
		return CartResponseDto.builder()
				.message("Product addded to your cart successfully")
				.build();
	}

}
