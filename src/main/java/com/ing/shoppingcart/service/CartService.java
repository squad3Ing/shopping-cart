package com.ing.shoppingcart.service;

import com.ing.shoppingcart.dto.CartRequestDto;
import com.ing.shoppingcart.dto.CartResponseDto;

public interface CartService {

	CartResponseDto addProductToCart(CartRequestDto cartRequestDto);

}
