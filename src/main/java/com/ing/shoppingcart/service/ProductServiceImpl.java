package com.ing.shoppingcart.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ing.shoppingcart.dao.ProductDao;
import com.ing.shoppingcart.dto.ProductResponseDto;
import com.ing.shoppingcart.entity.Product;
import com.ing.shoppingcart.exception.ProductNotFoundException;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductDao productDao;

	@Override
	public List<ProductResponseDto> searchProducts(String productName, int pageSize, int pageNumber) {
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		List<Product> listProduct = productDao.findByProductNameContaining(productName, pageable);

		if (listProduct.isEmpty())
			throw new ProductNotFoundException("No product exist with name " + productName);

		List<ProductResponseDto> listProductResponseDto = new ArrayList<ProductResponseDto>();
		listProduct.stream().forEach(product -> {
			ProductResponseDto productResponseDto = new ProductResponseDto();
			BeanUtils.copyProperties(product, productResponseDto);
			listProductResponseDto.add(productResponseDto);
		});
		return listProductResponseDto;
	}

}
