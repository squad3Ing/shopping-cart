package com.ing.shoppingcart.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ing.shoppingcart.dao.OrderItemsDao;
import com.ing.shoppingcart.dao.UserDao;
import com.ing.shoppingcart.dao.UserOrderDao;
import com.ing.shoppingcart.dto.LoginRequestDto;
import com.ing.shoppingcart.dto.LoginResponseDto;
import com.ing.shoppingcart.dto.OrderResponseDto;
import com.ing.shoppingcart.dto.ProductDto;
import com.ing.shoppingcart.entity.OrderItems;
import com.ing.shoppingcart.entity.User;
import com.ing.shoppingcart.entity.UserOrder;
import com.ing.shoppingcart.exception.OrderNotFoundException;
import com.ing.shoppingcart.exception.UserNotFoundException;
import com.ing.shoppingcart.util.ExceptionConstants;

@Service
public class UserServiceImpl implements UserService, Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	UserDao userDao;

	@Autowired
	UserOrderDao userOrderDao;
	
	@Autowired
	OrderItemsDao orderItemsDao;

	@Override
	public LoginResponseDto loginUser(LoginRequestDto requestDto) {
		User response = userDao.findByUserEmailAndPassword(requestDto.getUserEmail(), requestDto.getPassword());
		if (response == null) {
			throw new UserNotFoundException(ExceptionConstants.USER_NOT_FOUND);
		}
		return LoginResponseDto.builder().userId(response.getUserId()).message("logged in successfully").build();

	}

	@Override
	@Transactional
	public List<OrderResponseDto> getOrders(int userId, int pageSize, int pageNumber) {
		Optional<User> user = userDao.findByUserId(userId);
		if (!user.isPresent()) {
			throw new UserNotFoundException(ExceptionConstants.USER_NOT_FOUND);
		}

		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		List<UserOrder> listUserOrder = userOrderDao.findByUserId(userId, pageable);
		if (listUserOrder.isEmpty())
			throw new OrderNotFoundException("No orders exist for given user " + userId);

		List<OrderResponseDto> listOrderResponseDto = new ArrayList<OrderResponseDto>();
		listUserOrder.stream().forEach(userOrder -> {
			OrderResponseDto orderResponseDto = new OrderResponseDto();
			BeanUtils.copyProperties(userOrder, orderResponseDto);
			
			List<ProductDto> listProductDto = getOrderItems(userOrder.getOrderId());
			if (!listProductDto.isEmpty())
				orderResponseDto.setListProductDto(listProductDto);
			listOrderResponseDto.add(orderResponseDto);
		});
		return listOrderResponseDto;
	}

	@Transactional
	public List<ProductDto> getOrderItems(int orderId) {
		List<OrderItems> listOrderItems= orderItemsDao.findByOrderId(orderId);
		List<ProductDto> listProductDto = new ArrayList<ProductDto>();
		
		listOrderItems.stream().forEach(orderItems -> {
			ProductDto productDto = new ProductDto();
			BeanUtils.copyProperties(orderItems, productDto);
			listProductDto.add(productDto);
		});
		return listProductDto;
	}

}
