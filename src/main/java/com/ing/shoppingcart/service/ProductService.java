package com.ing.shoppingcart.service;

import java.util.List;

import com.ing.shoppingcart.dto.ProductResponseDto;

public interface ProductService {

	public List<ProductResponseDto> searchProducts(String productName, int pageSize, int pageNumber);

}
