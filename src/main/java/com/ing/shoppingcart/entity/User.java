package com.ing.shoppingcart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table
@Getter
@Setter
public class User {
	@Id@GeneratedValue(strategy = GenerationType.AUTO)
	 private Integer userId;
	 private String userName;
	 private String userEmail;
	 private String phone;
	 private String address;
	 private String password;
}
