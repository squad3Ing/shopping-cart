package com.ing.shoppingcart.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table
@Getter
@Setter
public class OrderItems {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer orderItemsId;
	private Integer orderId;
	private Integer productId;
	private Integer productQuantity;
}
