package com.ing.shoppingcart.util;

public class ExceptionConstants {
	private ExceptionConstants() {

	}
	public static final String USER_NOT_FOUND = "Invalid credentials";
	public static final String PRODUCT_NOT_FOUND = "The requested product is not available";
	public static final String FAILED_TO_ADD_CART = "Add prouct to cart is failed";
}
