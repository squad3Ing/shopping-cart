package com.ing.shoppingcart.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ing.shoppingcart.validator.impl.PageNumberConstraintValidator;

@Target({ ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PageNumberConstraintValidator.class)
public @interface PageNumberConstraint {
	String message() default "Invalid Page number";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
