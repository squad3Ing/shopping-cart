package com.ing.shoppingcart.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ing.shoppingcart.validator.impl.ProductNameConstraintValidator;

@Target({ ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ProductNameConstraintValidator.class)
public @interface ProductNameConstraint {
	String message() default "Invalid Product name";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
