package com.ing.shoppingcart.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.ing.shoppingcart.validator.impl.PageSizeConstraintValidator;

@Target({ ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PageSizeConstraintValidator.class)
public @interface PageSizeConstraint {
	String message() default "Invalid Page Size";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
