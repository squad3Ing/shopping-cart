package com.ing.shoppingcart.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ing.shoppingcart.exception.InvalidProductNameException;
import com.ing.shoppingcart.validator.ProductNameConstraint;

public class ProductNameConstraintValidator implements ConstraintValidator<ProductNameConstraint, String> {

	@Override
	public boolean isValid(String productName, ConstraintValidatorContext constraintValidatorContext) {
		if (productName == null)
			throw new InvalidProductNameException("product name may not be null");
		else if (productName.isEmpty())
			throw new InvalidProductNameException("product name may not be empty");
		else if (productName.length() < 4)
			throw new InvalidProductNameException("Requires four letter in product name");
		return true;
	}

}
