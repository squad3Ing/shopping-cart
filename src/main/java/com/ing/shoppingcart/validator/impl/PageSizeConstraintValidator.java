package com.ing.shoppingcart.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ing.shoppingcart.exception.InvalidPageSizeException;
import com.ing.shoppingcart.validator.PageSizeConstraint;

public class PageSizeConstraintValidator implements ConstraintValidator<PageSizeConstraint, Integer> {

	@Override
	public boolean isValid(Integer pagesize, ConstraintValidatorContext constraintValidatorContext) {
		if (pagesize == null)
			throw new InvalidPageSizeException("Page size may not be null");
		else if (pagesize < 1)
			throw new InvalidPageSizeException("Page size cannot be negative integer or zero");

		return true;
	}

}
