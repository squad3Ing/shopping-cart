package com.ing.shoppingcart.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.ing.shoppingcart.exception.InvalidPageNumberException;
import com.ing.shoppingcart.validator.PageNumberConstraint;

public class PageNumberConstraintValidator implements ConstraintValidator<PageNumberConstraint, Integer> {

	@Override
	public boolean isValid(Integer pageNumber, ConstraintValidatorContext constraintValidatorContext) {
		if (pageNumber == null)
			throw new InvalidPageNumberException("Page number may not be null");
		else if (pageNumber < 0)
			throw new InvalidPageNumberException("Page number cannot be negative integer");

		return true;
	}

}
