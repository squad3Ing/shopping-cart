package com.ing.shoppingcart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ing.shoppingcart.dto.LoginRequestDto;
import com.ing.shoppingcart.dto.LoginResponseDto;
import com.ing.shoppingcart.dto.OrderResponseDto;
import com.ing.shoppingcart.service.UserService;
import com.ing.shoppingcart.validator.PageNumberConstraint;
import com.ing.shoppingcart.validator.PageSizeConstraint;

@RestController
@RequestMapping("/users")
@Validated
public class UserController {

	@Autowired
	UserService userService;

	@PostMapping("/login")
	public ResponseEntity<LoginResponseDto> login(@RequestBody LoginRequestDto requestDto) {
		LoginResponseDto loginResponseDto = userService.loginUser(requestDto);
		return new ResponseEntity<LoginResponseDto>(loginResponseDto, HttpStatus.OK);

	}

	@GetMapping("/{userId}/orders")
	public ResponseEntity<List<OrderResponseDto>> getOrders(@PathVariable(name = "userId") int userId,
			@RequestParam(name = "pageNumber") @PageNumberConstraint int pageNumber, @RequestParam(name = "pageSize") @PageSizeConstraint int pageSize) {
		List<OrderResponseDto> listOrderResponseDto = userService.getOrders(userId, pageSize, pageNumber);
		return new ResponseEntity<List<OrderResponseDto>>(listOrderResponseDto, HttpStatus.OK);
	}

}
