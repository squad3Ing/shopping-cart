package com.ing.shoppingcart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ing.shoppingcart.dto.ProductResponseDto;
import com.ing.shoppingcart.service.ProductService;
import com.ing.shoppingcart.validator.PageNumberConstraint;
import com.ing.shoppingcart.validator.PageSizeConstraint;

@RestController
@RequestMapping("/products")
@Validated
public class ProductController {
	@Autowired
	ProductService productService;

	@GetMapping
	public ResponseEntity<List<ProductResponseDto>> searchProducts(@RequestParam(name = "name") String productName,
			@RequestParam(name = "pageNumber") @PageNumberConstraint int pageNumber,
			@RequestParam(name = "pageSize") @PageSizeConstraint int pageSize) {
		List<ProductResponseDto> listProductResponseDto = productService.searchProducts(productName, pageSize,
				pageNumber);
		return new ResponseEntity<List<ProductResponseDto>>(listProductResponseDto, HttpStatus.OK);
	}

}
