package com.ing.shoppingcart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ing.shoppingcart.dto.CartRequestDto;
import com.ing.shoppingcart.dto.CartResponseDto;
import com.ing.shoppingcart.service.CartService;

import lombok.extern.slf4j.Slf4j;



@RestController
@RequestMapping("/users")
@Slf4j
public class CartController {
	
	@Autowired
	CartService productService;
	
	@PostMapping("/carts")
	public ResponseEntity<CartResponseDto> addProductToCart(@RequestBody() CartRequestDto cartRequestDto) {
		log.info("Inside Cart controller");
		CartResponseDto response = productService.addProductToCart(cartRequestDto);
		return new ResponseEntity<CartResponseDto>(response,HttpStatus.OK);
	}
}
