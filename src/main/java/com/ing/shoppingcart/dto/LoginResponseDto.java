package com.ing.shoppingcart.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class LoginResponseDto {
	private String message;
	private Integer userId;
}
