package com.ing.shoppingcart.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderResponseDto {
	private int orderId;
	private float orderAmount;
	private LocalDate orderDate;
	private LocalDate deliveryDate;
	private String status;
	private List<ProductDto> listProductDto = new ArrayList<ProductDto>();

}
