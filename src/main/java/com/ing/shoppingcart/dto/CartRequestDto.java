package com.ing.shoppingcart.dto;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CartRequestDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer userId;
	private Integer productId;
	private Integer productQuantity;
}
