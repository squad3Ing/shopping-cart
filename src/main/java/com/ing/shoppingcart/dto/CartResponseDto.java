package com.ing.shoppingcart.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CartResponseDto {
	private String message;
}
