package com.ing.shoppingcart.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class LoginRequestDto {
	
	 private String userEmail;
	 private String password;
}
