package com.ing.shoppingcart.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductResponseDto {
	private Integer productId;
	private String productName;
	private String productDescription;
	private Float price;
	private Integer quantity;
}
