package com.ing.shoppingcart.exception;

public class InvalidPageNumberException extends RuntimeException {

	private static final long serialVersionUID = -6646677737783163817L;

	public InvalidPageNumberException(String message) {
		super(message);
	}
}
