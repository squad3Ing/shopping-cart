package com.ing.shoppingcart.exception;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<ResponseError> userNotFoundException(Exception e) {
		ResponseError error = new ResponseError(e.getMessage(), HttpStatus.NOT_FOUND.value());
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(ProductNotFoundException.class)
	public ResponseEntity<ResponseError> productNotFoundException(Exception e) {
		ResponseError error = new ResponseError(e.getMessage(), HttpStatus.NOT_FOUND.value());
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(AddToCartFailedException.class)
	public ResponseEntity<ResponseError> addToCartFailedException(Exception e) {
		ResponseError error = new ResponseError(e.getMessage(), HttpStatus.NOT_FOUND.value());
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(OrderNotFoundException.class)
	public ResponseEntity<ResponseError> handleOrderNotFoundException(OrderNotFoundException orderNotFoundException) {
		ResponseError error = new ResponseError(orderNotFoundException.getMessage(), HttpStatus.NOT_FOUND.value());
		return new ResponseEntity<ResponseError>(error, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(value = ConstraintViolationException.class)
	public ResponseEntity<ResponseError> handleConstraintViolationException(
			ConstraintViolationException constraintViolationException) {
		ResponseError error = new ResponseError(constraintViolationException.getMessage(),
				HttpStatus.BAD_REQUEST.value());
		return new ResponseEntity<ResponseError>(error, HttpStatus.BAD_REQUEST);
	}

}
