package com.ing.shoppingcart.exception;

public class InvalidPageSizeException extends RuntimeException {

	private static final long serialVersionUID = -1217018320781794787L;

	public InvalidPageSizeException(String message) {
		super(message);
	}
}
