package com.ing.shoppingcart.exception;

import java.io.Serializable;

public class AddToCartFailedException extends RuntimeException implements Serializable{

	private static final long serialVersionUID = 1L;

		public AddToCartFailedException(String message) {
			super(message);
		}
}