package com.ing.shoppingcart.exception;

import java.io.Serializable;

public class OrderNotFoundException extends RuntimeException implements Serializable {

	private static final long serialVersionUID = -2027109401288506424L;

	public OrderNotFoundException(String message) {
		super(message);
	}

}
