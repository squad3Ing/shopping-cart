package com.ing.shoppingcart.exception;

public class InvalidProductNameException extends RuntimeException {

	private static final long serialVersionUID = 8369216760654418988L;

	public InvalidProductNameException(String message) {
		super(message);
	}
}
