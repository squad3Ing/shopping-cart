package com.ing.shoppingcart.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ing.shoppingcart.entity.OrderItems;

public interface OrderItemsDao extends JpaRepository<OrderItems, Integer> {
	public List<OrderItems> findByOrderId(Integer orderId);
}
