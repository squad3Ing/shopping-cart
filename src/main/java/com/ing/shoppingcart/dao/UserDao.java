package com.ing.shoppingcart.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ing.shoppingcart.entity.User;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {

	User findByUserEmailAndPassword(String userEmail, String password);
	
	public Optional<User> findByUserId(int userId);

}
