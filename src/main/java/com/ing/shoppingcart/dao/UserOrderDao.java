package com.ing.shoppingcart.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ing.shoppingcart.entity.UserOrder;

public interface UserOrderDao extends JpaRepository<UserOrder, Integer> {

	public List<UserOrder> findByUserId(int userId, Pageable pageable);

}
