package com.ing.shoppingcart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ing.shoppingcart.entity.CartItems;

@Repository
public interface CartDao extends JpaRepository<CartItems, Integer> {

}
