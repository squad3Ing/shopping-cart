package com.ing.shoppingcart.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ing.shoppingcart.entity.Product;

@Repository
public interface ProductDao extends JpaRepository<Product, Integer> {

	Product findByProductId(Integer productId);

	public List<Product> findByProductNameContaining(String productName, Pageable pageable);

}
